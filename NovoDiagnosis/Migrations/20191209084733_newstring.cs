﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class newstring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AuthorizationStatus",
                table: "Provider",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AuthorizationStatus",
                table: "Provider",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
