﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class provider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedBy",
                table: "ClaimBatch");

            migrationBuilder.DropColumn(
                name: "AssignedDate",
                table: "ClaimBatch");

            migrationBuilder.DropColumn(
                name: "Assignedto",
                table: "ClaimBatch");

            migrationBuilder.DropColumn(
                name: "InitiallyAssigned",
                table: "ClaimBatch");

            migrationBuilder.DropColumn(
                name: "Provider",
                table: "Appointment");

            migrationBuilder.AddColumn<int>(
                name: "ProviderId",
                table: "Appointment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Appointment_ProviderId",
                table: "Appointment",
                column: "ProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointment_Provider_ProviderId",
                table: "Appointment",
                column: "ProviderId",
                principalTable: "Provider",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointment_Provider_ProviderId",
                table: "Appointment");

            migrationBuilder.DropIndex(
                name: "IX_Appointment_ProviderId",
                table: "Appointment");

            migrationBuilder.DropColumn(
                name: "ProviderId",
                table: "Appointment");

            migrationBuilder.AddColumn<string>(
                name: "AssignedBy",
                table: "ClaimBatch",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "AssignedDate",
                table: "ClaimBatch",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Assignedto",
                table: "ClaimBatch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InitiallyAssigned",
                table: "ClaimBatch",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Provider",
                table: "Appointment",
                nullable: false,
                defaultValue: "");
        }
    }
}
