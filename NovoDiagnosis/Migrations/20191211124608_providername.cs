﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class providername : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "InitiallyAssignedDate",
                table: "ClaimBatch");

            migrationBuilder.AlterColumn<int>(
                name: "StateId",
                table: "Provider",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "ProviderName",
                table: "Claim",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider",
                column: "StateId",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "ProviderName",
                table: "Claim");

            migrationBuilder.AlterColumn<int>(
                name: "StateId",
                table: "Provider",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InitiallyAssignedDate",
                table: "ClaimBatch",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider",
                column: "StateId",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
