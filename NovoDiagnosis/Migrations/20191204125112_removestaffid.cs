﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class removestaffid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enrollee_Staff_staffId",
                table: "Enrollee");

            migrationBuilder.DropIndex(
                name: "IX_Enrollee_staffId",
                table: "Enrollee");

            migrationBuilder.DropColumn(
                name: "staffId",
                table: "Enrollee");

            migrationBuilder.AlterColumn<string>(
                name: "Passphrase",
                table: "Enrollee",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Passphrase",
                table: "Enrollee",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "staffId",
                table: "Enrollee",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enrollee_staffId",
                table: "Enrollee",
                column: "staffId");

            migrationBuilder.AddForeignKey(
                name: "FK_Enrollee_Staff_staffId",
                table: "Enrollee",
                column: "staffId",
                principalTable: "Staff",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
