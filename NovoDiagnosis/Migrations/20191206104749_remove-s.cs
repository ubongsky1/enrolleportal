﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class removes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanySubsidiaries",
                table: "CompanySubsidiaries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyPlans",
                table: "CompanyPlans");

            migrationBuilder.RenameTable(
                name: "CompanySubsidiaries",
                newName: "CompanySubsidiary");

            migrationBuilder.RenameTable(
                name: "CompanyPlans",
                newName: "CompanyPlan");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanySubsidiary",
                table: "CompanySubsidiary",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyPlan",
                table: "CompanyPlan",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanySubsidiary",
                table: "CompanySubsidiary");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyPlan",
                table: "CompanyPlan");

            migrationBuilder.RenameTable(
                name: "CompanySubsidiary",
                newName: "CompanySubsidiaries");

            migrationBuilder.RenameTable(
                name: "CompanyPlan",
                newName: "CompanyPlans");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanySubsidiaries",
                table: "CompanySubsidiaries",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyPlans",
                table: "CompanyPlans",
                column: "Id");
        }
    }
}
