﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NovoDiagnosis.Migrations
{
    public partial class more : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Assignee",
                table: "Provider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "AuthorizationNote",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AuthorizationStatus",
                table: "Provider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AuthorizedBy",
                table: "Provider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "AuthorizedDate",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyConsession",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletionNote",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DelistNote",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DisapprovalDate",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DisapprovalNote",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DisapprovedBy",
                table: "Provider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LgaId",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Parentid",
                table: "Provider",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "PaymentEmail1",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentEmail2",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone2",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProviderTariffs",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Providergpscordinate",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Providerplans",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Providerservices",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "Provider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SubCode",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "delistedBy",
                table: "Provider",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "delisteddate",
                table: "Provider",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isDelisted",
                table: "Provider",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Lgas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    State = table.Column<long>(nullable: false),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lgas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProviderServices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    providerId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    OpeningDays = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProviderServices_Provider_providerId",
                        column: x => x.providerId,
                        principalTable: "Provider",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Provider_LgaId",
                table: "Provider",
                column: "LgaId");

            migrationBuilder.CreateIndex(
                name: "IX_Provider_StateId",
                table: "Provider",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_ProviderServices_providerId",
                table: "ProviderServices",
                column: "providerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Provider_Lgas_LgaId",
                table: "Provider",
                column: "LgaId",
                principalTable: "Lgas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider",
                column: "StateId",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Provider_Lgas_LgaId",
                table: "Provider");

            migrationBuilder.DropForeignKey(
                name: "FK_Provider_States_StateId",
                table: "Provider");

            migrationBuilder.DropTable(
                name: "Lgas");

            migrationBuilder.DropTable(
                name: "ProviderServices");

            migrationBuilder.DropIndex(
                name: "IX_Provider_LgaId",
                table: "Provider");

            migrationBuilder.DropIndex(
                name: "IX_Provider_StateId",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Area",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Assignee",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "AuthorizationNote",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "AuthorizationStatus",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "AuthorizedBy",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "AuthorizedDate",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "CompanyConsession",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "DeletionNote",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "DelistNote",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "DisapprovalDate",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "DisapprovalNote",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "DisapprovedBy",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "LgaId",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Parentid",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "PaymentEmail1",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "PaymentEmail2",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Phone2",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "ProviderTariffs",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Providergpscordinate",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Providerplans",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Providerservices",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "SubCode",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "delistedBy",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "delisteddate",
                table: "Provider");

            migrationBuilder.DropColumn(
                name: "isDelisted",
                table: "Provider");
        }
    }
}
