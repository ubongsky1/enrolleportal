﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models
{
    [Table("Provider")]
    public partial class Provider
    {

        public int Id { get; set; }
        public  string Name { get; set; }
        public  string Code { get; set; }
        public  string SubCode { get; set; }
        public  string Email { get; set; }
        public  string Phone { get; set; }
        public  string Phone2 { get; set; }
        public  string Website { get; set; }
        public  string Address { get; set; }
        public  string Area { get; set; }
        public int? StateId { get; set; }
        public  State State { get; set; }
        public  Lga Lga { get; set; }
        public  string PaymentEmail1 { get; set; }
        public  string PaymentEmail2 { get; set; }
        public  int Assignee { get; set; }
        public  string Providergpscordinate { get; set; }
        public  string Providerservices { get; set; }
        public  string Providerplans { get; set; }
        public  string ProviderTariffs { get; set; }
        public  string CreatedBy { get; set; }
        public  string AuthorizationStatus { get; set; }
        public  string AuthorizationNote { get; set; }
        public  string DisapprovalNote { get; set; }
        public  string AuthorizedBy { get; set; }
        public  string DisapprovedBy { get; set; }
        public  DateTime? AuthorizedDate { get; set; }
        public  DateTime? DisapprovalDate { get; set; }
        public  string DeletionNote { get; set; }
        public  long Parentid { get; set; }
        public  bool Status { get; set; }
        public  IList<ProviderServices> ProviderServices { get; set; }
        public  bool isDelisted { get; set; }
        public  string DelistNote { get; set; }
        public  DateTime? delisteddate { get; set; }
        public  string delistedBy { get; set; }
        public  string CompanyConsession { get; set; }
        public bool IsDeleted { get; set; }


    }
}
