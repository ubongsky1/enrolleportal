﻿using NovoEnrollee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models.ViewModels
{
    public class BenefitModel
    {
        public int Staffs { get; set; }
        public int Enrollees { get; set; }
        public int Benefits { get; set; }
        public int Company { get; set; }
        public int CompanyPlans { get; set; }
        public int CompanyBenefit { get; set; }
        public int Notifications { get; set; }
        public int Providers { get; set; }
        public int Appointments { get; set; }
        public int Visithospital { get; set; }


        public IEnumerable<Benefit> MyBenefits { get; set; }
        public IEnumerable<CompanyPlan> MyCompanyPlans { get; set; }
        public IEnumerable<CompanyBenefit> MyCompanyBenefits { get; set; }
        public IEnumerable<Notification> MyNotifications { get; set; }
        public IEnumerable<Provider> MyProviders { get; set; }
        public IEnumerable<Appointment> MyAppointments { get; set; }
        public IEnumerable<Claim> MyClaims { get; set; }


        public BenefitModel()
        {
            MyBenefits = new List<Benefit>();
            MyCompanyPlans = new List<CompanyPlan>();
            MyCompanyBenefits = new List<CompanyBenefit>();
            MyNotifications = new List<Notification>();
            MyProviders = new List<Provider>();
            MyAppointments = new List<Appointment>();
            MyClaims = new List<Claim>();
        }
    }
}
