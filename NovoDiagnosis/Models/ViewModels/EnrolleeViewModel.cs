﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models.ViewModels
{
    public class EnrolleeViewModel
    {
        public Enrollee Enrollee { get; set; }

        public EnrolleePassport EnrolleePassport { get; set; }

        public Staff Staff { get; set; }

        public Provider Provider { get; set; }

        public State State { get; set; }

        public Company Company { get; set; }

        public CompanyPlan CompanyPlan { get; set; }

        public CompanySubsidiary CompanySubsidiary { get; set; }
    }
}
