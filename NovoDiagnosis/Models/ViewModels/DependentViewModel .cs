﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models.ViewModels
{
    public class DependentViewModel
    {
        public IEnumerable<Enrollee> Enrollee { get; set; }

        public IEnumerable<EnrolleePassport> EnrolleePassport { get; set; }

        public IEnumerable<Staff> Staff { get; set; }

        public IEnumerable<Provider> Provider { get; set; }

        public IEnumerable<State> State { get; set; }

        public IEnumerable<Company> Company { get; set; }

        public IEnumerable<CompanyPlan> CompanyPlan { get; set; }

        public IEnumerable<CompanySubsidiary> CompanySubsidiary { get; set; }
    }
}
