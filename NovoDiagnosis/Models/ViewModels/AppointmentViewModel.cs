﻿using NovoEnrollee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models.ViewModels
{
    public class AppointmentViewModel
    {
       

        public Provider Providers { get; set; }

        public Staff Staffs { get; set; }

        public Company Companies { get; set; }

        public State States { get; set; }

        public Enrollee Enrollee { get; set; }

        public List<CompanyBenefit> CompanyBenefits { get; set; }

        public List<Benefit> Benefits { get; set; }

        public List<CompanyPlan> CompanyPlans { get; set; }

        public List<Enrollee> Enrollees { get; set; }

     
    }
}
