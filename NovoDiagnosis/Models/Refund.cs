﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models
{
    public class Refund
    {
        public int Id { get; set; }

        public string Reason { get; set; }

        public int ProviderId { get; set; }
        public Provider Provider { get; set; }

        public string Complaint { get; set; }

        public string Diagnosis { get; set; }

        public string Treatment { get; set; }

        public double TreatmentBill { get; set; }

        public double PayByEnrollee { get; set; }

        public double AmountRefunded { get; set; }

        public int BankId { get; set; }
        public Bank Bank { get; set; }

        public string AccountName { get; set; }

        public string AccountNo { get; set; }

        public string AccountType { get; set; }

        public double AmountPaid { get; set; }

        public string PeocessedBy { get; set; }

        public DateTime PaidDate { get; set; }

        public string EnrolleeRemark { get; set; }

        public string Status { get; set; }

        public string NovoRemark { get; set; }

        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }


    }
}
