﻿namespace NovoDiagnosis.Models
{
    public class ProviderServices
    {

        public int Id { get; set; }
        public  Provider provider { get; set; }
        public  string Name { get; set; }
        public  string description { get; set; }
        public  string OpeningDays { get; set; }

    }
}