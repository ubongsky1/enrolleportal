﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models
{
    public class ApplicationUser : IdentityUser
    {

        public ApplicationUser() : base() { }

        public string FullName { get; set; }
        public string StaffId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Policynumber { get; set; }
    }
}
