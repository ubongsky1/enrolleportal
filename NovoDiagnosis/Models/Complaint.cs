﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models
{
    public class Complaint
    {
        public int Id { get; set; }

        public string ClaimId { get; set; }

        public string Subject { get; set; }

        public string Comment { get; set; }

        public string TreatedBy { get; set; }

        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

    }
}
