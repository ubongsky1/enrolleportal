﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoDiagnosis.Models
{


    public enum Status
    {

        Active = 1,
        Suspended = 2,
        Cancelled = 3,
        Completed = 4,
        Read = 5,
        Unread = 6


    }

}
