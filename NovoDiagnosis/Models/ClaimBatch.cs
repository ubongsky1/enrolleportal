﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NovoEnrollee.Models
{
    [Table("ClaimBatch")]
    public class ClaimBatch
    {
        public ClaimBatch()
        {
            Claim = new HashSet<Claim>();
            IncomingClaims = new HashSet<IncomingClaims>();
        }

        public int Id { get; set; }
        public int? ProviderId { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Batch { get; set; }
        public string Status { get; set; }
        public int? SubmitedVetbyUser { get; set; }
        public int? SubmitedReviewbyUser { get; set; }
        public DateTime? SubmitedForReviewDate { get; set; }
        public DateTime? ReviewDate { get; set; }
        public int? ReviewedBy { get; set; }
        public DateTime? VetDate { get; set; }
        public DateTime? SubmitedForPaymentDate { get; set; }
        public int? SubmitedPaymentbyUser { get; set; }
        public string AuthorizationStatus { get; set; }
        public string AuthorizationNote { get; set; }
        public string DisapprovalNote { get; set; }
        public int? AuthorizedBy { get; set; }
        public int? DisapprovedBy { get; set; }
        public DateTime? AuthorizedDate { get; set; }
        public DateTime? DisapprovalDate { get; set; }
        public string DeletionNote { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public string ProviderName { get; set; }
        public int? Claimscountfromclient { get; set; }
        public bool? Isremote { get; set; }

        //public DateTime InitiallyAssignedDate { get; set; }
        public ICollection<Claim> Claim { get; set; }
        public ICollection<IncomingClaims> IncomingClaims { get; set; }
    }
}
