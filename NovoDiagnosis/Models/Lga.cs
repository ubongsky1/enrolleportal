﻿namespace NovoDiagnosis.Models
{
    public class Lga
    {
        public int Id { get; set; }
        public  string Name { get; set; }
        public  long State { get; set; }
        public  bool Status { get; set; }
    }
}