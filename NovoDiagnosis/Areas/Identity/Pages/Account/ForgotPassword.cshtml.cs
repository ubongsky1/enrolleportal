﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NovoDiagnosis.Models;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace NovoDiagnosis.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public ForgotPasswordModel(UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        public async Task<IActionResult> OnPostAsync(string Email)
        {
          

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.Email);
               
               if (user == null)
                {

                    return RedirectToPage("./ForgotPassword");
                }

                ////For more information on how to enable account confirmation and password reset please
                ////visit https://go.microsoft.com/fwlink/?LinkID=532713
                //var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                //var callbackUrl = Url.Page(
                //    "/Account/ResetPassword",
                //    pageHandler: null,
                //    values: new { code },
                //    protocol: Request.Scheme);

                //await _emailSender.SendEmailAsync(
                //    Input.Email,
                //    "Reset Password",
                //    $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");



                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ResetPassword",
                        pageHandler: null,
                        values: new { code },
                        protocol: Request.Scheme);
                    //instantiate a new MimeMessage
                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(user.Email));

                    message.Bcc.Add(new MailboxAddress("novohealthafrica@outlook.com"));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Name", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "Password Reset Link";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<p>Dear " + user.FullName + ",</p><br/>" +
                        "<p>Thank you for requesting for Password reset. Kindly click on the link below to reset your Password</p>" +
                          $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>." +
                          "<p><br/><br/><br><br/><p>Thank you for choosing Novo Health Africa</p></p>"


                    };

                    //Configure the e-mail
                    using (var emailClient = new SmtpClient())
                    {
                        emailClient.Connect("smtp.office365.com", 587, false);
                        emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                
                
                return RedirectToPage("./ForgotPasswordConfirmation");
               

            }


            return Page();
        }
    }
}
