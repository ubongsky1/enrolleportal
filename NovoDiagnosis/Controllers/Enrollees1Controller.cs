﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class Enrollees1Controller : Controller
    {
        private readonly ApplicationDbContext _context;

        public Enrollees1Controller(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Enrollees1
        public async Task<IActionResult> Index()
        {
            return View(await _context.Enrollees.ToListAsync());
        }

        // GET: Enrollees1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrollee == null)
            {
                return NotFound();
            }

            return View(enrollee);
        }

        // GET: Enrollees1/Create
        public IActionResult Create(int? id, string StaffId, string StaffFullname, string Company)
        {
            TempData["StaffName"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffFullname).FirstOrDefault();
            TempData["Staffplanid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            TempData["Staffprofileid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.Id).FirstOrDefault();
            var staffplanid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            var Planid = _context.CompanyPlan.Where(x => x.Id == staffplanid).Select(x => x.Planid).FirstOrDefault();

            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            var Providerlist = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.State = new SelectList(_context.States, "Id", "Name");
            var Statelist = new SelectList(_context.States, "Id", "Name");


            TempData["Id"] = id;
            ViewBag.Providers = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewData["Providerid"] = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.States = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Stateid"] = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Lgas = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Lgaid"] = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");

            ViewBag.Companyid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.CompanyId).FirstOrDefault();

            return View();
        }

        // POST: Enrollees1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase,SiteId")] Enrollee enrollee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enrollee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(enrollee);
        }

        // GET: Enrollees1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees.FindAsync(id);
            if (enrollee == null)
            {
                return NotFound();
            }
            return View(enrollee);
        }

        // POST: Enrollees1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase,SiteId")] Enrollee enrollee)
        {
            if (id != enrollee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(enrollee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnrolleeExists(enrollee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(enrollee);
        }

        // GET: Enrollees1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrollee == null)
            {
                return NotFound();
            }

            return View(enrollee);
        }

        // POST: Enrollees1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var enrollee = await _context.Enrollees.FindAsync(id);
            _context.Enrollees.Remove(enrollee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EnrolleeExists(int id)
        {
            return _context.Enrollees.Any(e => e.Id == id);
        }
    }
}
