﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoDiagnosis.Models.ViewModels;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{

    
    public class DependentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public DependentsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

       

        // GET: Dependents
        public async Task<IActionResult> Index()
        {

           
                var user = await _userManager.GetUserAsync(User);

                DependentViewModel enrolleeViewModel = new DependentViewModel();
                enrolleeViewModel.Enrollee = (
                             from c in _context.Staffs
                             join d in _context.Enrollees on c.Profileid equals d.Parentid
                             join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                             where c.StaffId == user.StaffId && c.IsDeleted == false
                             select d).ToList();

                enrolleeViewModel.Staff = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Id
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select c).ToList();
                enrolleeViewModel.Company = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Id
                           join f in _context.Company on d.Companyid equals f.Id
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select f).ToList();
                enrolleeViewModel.CompanyPlan = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Id
                           join f in _context.Company on d.Companyid equals f.Id
                           join g in _context.CompanyPlan on d.Subscriptionplanid equals g.Id
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select g).ToList();
                enrolleeViewModel.Provider = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Id
                           join h in _context.Providers on d.Primaryprovider equals h.Id
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select h).ToList();
                enrolleeViewModel.EnrolleePassport = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Parentid
                           join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select e).ToList();
                enrolleeViewModel.CompanySubsidiary = (
                           from c in _context.Staffs
                           join i in _context.CompanySubsidiary on c.CompanySubsidiary equals i.Id
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select i).ToList();

                enrolleeViewModel.State = (
                          from c in _context.Staffs
                          join d in _context.Enrollees on c.Profileid equals d.Id
                          join j in _context.States on d.Stateid equals j.Id
                          where c.StaffId == user.StaffId && c.IsDeleted == false
                          select j).ToList();


                ViewBag.enrolleeimg = (
                           from c in _context.Staffs
                           join d in _context.Enrollees on c.Profileid equals d.Parentid
                           join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                           where c.StaffId == user.StaffId && c.IsDeleted == false
                           select e).ToList();

            //Convert.ToBase64String(enrolleeViewModel.EnrolleePassport.Imgraw).ToList();


            return View(enrolleeViewModel);
            }
        }

        //// GET: Dependents/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var enrollee = await _context.Enrollees
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (enrollee == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(enrollee);
        //}

        //// GET: Dependents/Create
        //public IActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Dependents/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase")] Enrollee enrollee)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(enrollee);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(enrollee);
        //}

        //// GET: Dependents/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var enrollee = await _context.Enrollees.FindAsync(id);
        //    if (enrollee == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(enrollee);
        //}

        //// POST: Dependents/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase")] Enrollee enrollee)
        //{
        //    if (id != enrollee.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(enrollee);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!EnrolleeExists(enrollee.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(enrollee);
        //}

        //// GET: Dependents/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var enrollee = await _context.Enrollees
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (enrollee == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(enrollee);
        //}

        //// POST: Dependents/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var enrollee = await _context.Enrollees.FindAsync(id);
        //    _context.Enrollees.Remove(enrollee);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool EnrolleeExists(int id)
        //{
        //    return _context.Enrollees.Any(e => e.Id == id);
        //}
    }

