﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;
using NovoEnrollee.Models;

namespace NovoDiagnosis.Controllers
{
    public class MedicalHistoryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public MedicalHistoryController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: MedicalHistory
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            //var mhistory = _context.Claims.Where(x => x.EnrolleePolicyNumber.Contains(user.Policynumber)).Join(x => x.);
            var history = (
                         from c in _context.Claims 
                         join d in _context.Providers on c.ProviderId equals d.Id
                         where c.EnrolleePolicyNumber.Contains(user.Policynumber)
                         select new Claim
                         {
                             Id = c.Id,
                             EnrolleeFullname = c.EnrolleeFullname,
                             EnrolleePolicyNumber = c.EnrolleePolicyNumber,
                             EnrolleeCompanyName = c.EnrolleeCompanyName,
                             ServiceDate = c.ServiceDate,
                             ProviderName = d.Name,
                             AdmissionDate = c.AdmissionDate,
                             DischargeDate = c.DischargeDate,
                             Diagnosis = c.Diagnosis
                         }).ToList();

            ViewData["MedicalHistory"] = history;

            return View();
        }

       
    }
}
