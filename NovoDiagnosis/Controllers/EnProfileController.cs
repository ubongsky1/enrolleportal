﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoDiagnosis.Models.ViewModels;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class EnProfileController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EnProfileController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EnProfile
        public async Task<IActionResult> Index()
        {
            return View(await _context.Enrollees.ToListAsync());
        }


        public IActionResult Create(int? id, string StaffId, string StaffFullname, string Company)
        {
            TempData["StaffName"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffFullname).FirstOrDefault();
            TempData["Staffplanid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            TempData["Staffprofileid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.Id).FirstOrDefault();
            var staffplanid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            var Planid = _context.CompanyPlan.Where(x => x.Id == staffplanid).Select(x => x.Planid).FirstOrDefault();

            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            var Providerlist = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.State = new SelectList(_context.States, "Id", "Name");
            var Statelist = new SelectList(_context.States, "Id", "Name");


            TempData["Id"] = id;
            ViewBag.Providers = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewData["Providerid"] = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.States = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Stateid"] = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Lgas = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Lgaid"] = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");

            ViewBag.Companyid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.CompanyId).FirstOrDefault();

            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Enrollee enrollee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enrollee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(enrollee);
        }
        // GET: EnProfile/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrollee == null)
            {
                return NotFound();
            }

            return View(enrollee);
        }

        // GET: EnProfile/Create
        public IActionResult Add(int? id, string StaffId, string StaffFullname, string Company)
        {

            TempData["StaffName"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffFullname).FirstOrDefault();
            TempData["Staffplanid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            TempData["Staffprofileid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.Id).FirstOrDefault();
            var staffplanid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            var Planid = _context.CompanyPlan.Where(x => x.Id == staffplanid).Select(x => x.Planid).FirstOrDefault();

            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            var Providerlist = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.State = new SelectList(_context.States, "Id", "Name");
            var Statelist = new SelectList(_context.States, "Id", "Name");


            TempData["Id"] = id;
            ViewBag.Providers = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewData["Providerid"] = new SelectList(_context.Providers.OrderBy(x => x.Name).Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())), "Id", "Name");
            ViewBag.States = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Stateid"] = new SelectList(_context.States.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Lgas = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");
            ViewData["Lgaid"] = new SelectList(_context.Lgas.OrderBy(x => x.Name), "Id", "Name");

            ViewBag.Companyid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.CompanyId).FirstOrDefault();

            return View();
        }

        // POST: Enrollees1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase")] Enrollee enrollee, EnroViewModel enroViewModel, IFormFile Image)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enrollee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            //if (ModelState.IsValid)
            //{


            //    Enrollee model = new Enrollee()
            //    {
            //        Age = enrollee.Age,
            //        Staffprofileid = enrollee.Staffprofileid,
            //        Stateid = enrollee.Stateid,
            //        Status = enrollee.Status,
            //        Companyid = enrollee.Companyid,
            //        Createdby = enrollee.Createdby,
            //        CreatedOn = enrollee.CreatedOn,
            //        Dateexpunged = enrollee.Dateexpunged,
            //        Bulkjobid = enrollee.Bulkjobid,
            //        Datereceived = enrollee.Datereceived,
            //        Dob = enrollee.Dob,
            //        Emailaddress = enrollee.Emailaddress,
            //        EnrolleePassportId = enrollee.EnrolleePassportId,
            //        Expungedby = enrollee.Expungedby,
            //        ExpungeNote = enrollee.ExpungeNote,
            //        Guid = enrollee.Guid,
            //        Hasactivesubscription = enrollee.Hasactivesubscription,
            //        Hasdependents = enrollee.Hasdependents,
            //        HasRefPolicyNumber = enrollee.HasRefPolicyNumber,
            //        Id = enrollee.Id,
            //        IdCardPrinted = enrollee.IdCardPrinted,
            //        IsDeleted = enrollee.IsDeleted,
            //        Isexpundged = enrollee.Isexpundged,
            //        LastyearBirthdaymsgsent = enrollee.LastyearBirthdaymsgsent,
            //        Lgaid = enrollee.Lgaid,
            //        Maritalstatus = enrollee.Maritalstatus,
            //        Mobilenumber = enrollee.Mobilenumber,
            //        Mobilenumber2 = enrollee.Mobilenumber2,
            //        Occupation = enrollee.Occupation,
            //        Othernames = enrollee.Othernames,
            //        Parentid = enrollee.Parentid,
            //        Parentrelationship = enrollee.Parentrelationship,
            //        Passphrase = enrollee.Passphrase,
            //        Policynumber = enrollee.Policynumber,
            //        Preexistingmedicalhistory = enrollee.Preexistingmedicalhistory,
            //        Primaryprovider = enrollee.Primaryprovider,
            //        RefPolicynumber = enrollee.RefPolicynumber,
            //        Residentialaddress = enrollee.Residentialaddress,
            //        Sex = enrollee.Sex,
            //        Specialidcardfield1 = enrollee.Specialidcardfield1,
            //        Specialidcardfield2 = enrollee.Specialidcardfield2,
            //        Specialidcardfield3 = enrollee.Specialidcardfield3,
            //        Sponsorshiptype = enrollee.Sponsorshiptype,
            //        Sponsorshiptypenote = enrollee.Sponsorshiptypenote,
            //        Sponsorshiptypeothername = enrollee.Sponsorshiptypeothername,
            //        Subscriptionplanid = enrollee.Subscriptionplanid,
            //        Surname = enrollee.Surname,
            //        Title = enrollee.Title,
            //        UpdatedOn = enrollee.UpdatedOn,

            //    };
            //    _context.Enrollees.Add(model);
            //    await _context.SaveChangesAsync();

            //    //using (var ms = new MemoryStream())
            //    // {
            //    //     Image.CopyTo(ms);
            //    //     enroViewModel.Imgraw = ms.ToArray();
            //    //     enroViewModel.CreatedOn = enrollee.CreatedOn;
            //    //     enroViewModel.Enrolleeid = enrollee.Id;
            //    //     enroViewModel.Policynumber = enrollee.Policynumber;
            //    //     enroViewModel.IsDeleted = enrollee.IsDeleted;
            //    //     enroViewModel.UpdatedOn = enrollee.UpdatedOn;
            //    // }
            //    EnrolleePassport enroPassport = new EnrolleePassport();

            //     using (var ms = new MemoryStream())
            //     {
            //        Image.CopyTo(ms);
            //        enroPassport.CreatedOn = enrollee.CreatedOn;
            //        enroPassport.Enrolleeid = enrollee.Id;
            //        enroPassport.Enrolleepolicyno = enrollee.Policynumber;
            //        enroPassport.Imgraw = ms.ToArray();
            //        enroPassport.IsDeleted = enrollee.IsDeleted;
            //        enroPassport.UpdatedOn = enrollee.UpdatedOn;
            //     }

            // my code here 
            //  using(var stream = new MemoryStream()){
            //   Image.CopyTo(stream); 
            //    enrollee.Image = stream.ToArray();
            //    }
            //    _context.EnrolleePassport.Add(enroPassport);
            //    await _context.SaveChangesAsync();


            //    var staff = _context.Staffs.Where(x => x.Id == enrollee.Staffprofileid && x.HasProfile == false && x.IsDeleted == false).FirstOrDefault();
            //    staff.IsDeleted = false;
            //    staff.Profileid = enrollee.Id;
            //    staff.UpdatedOn = enrollee.UpdatedOn;
            //    _context.Entry(staff).Property("IsDeleted").IsModified = true;
            //    _context.Entry(staff).Property("Profileid").IsModified = true;
            //    _context.Entry(staff).Property("UpdatedOn").IsModified = true;
            //    await _context.SaveChangesAsync();


            //    return RedirectToAction(nameof(Index));
            //}
            return View(enrollee);
        }

        // GET: EnProfile/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees.FindAsync(id);
            if (enrollee == null)
            {
                return NotFound();
            }
            return View(enrollee);
        }

        // POST: EnProfile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase")] Enrollee enrollee)
        {
            if (id != enrollee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(enrollee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnrolleeExists(enrollee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(enrollee);
        }

        // GET: EnProfile/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrollee = await _context.Enrollees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrollee == null)
            {
                return NotFound();
            }

            return View(enrollee);
        }

        // POST: EnProfile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var enrollee = await _context.Enrollees.FindAsync(id);
            _context.Enrollees.Remove(enrollee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EnrolleeExists(int id)
        {
            return _context.Enrollees.Any(e => e.Id == id);
        }
    }
}
