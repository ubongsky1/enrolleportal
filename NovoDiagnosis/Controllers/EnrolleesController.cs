﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoDiagnosis.Models.ViewModels;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class EnrolleesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public EnrolleesController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            EnrolleeViewModel enrolleeViewModel = new EnrolleeViewModel();
            enrolleeViewModel.Enrollee = (
                         from c in _context.Staffs
                         join d in _context.Enrollees on c.Profileid equals d.Id
                         join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                         where c.StaffId == user.StaffId && c.IsDeleted == false
                         select d).FirstOrDefault();

            enrolleeViewModel.Staff = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Profileid equals d.Id
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select c).FirstOrDefault();
            enrolleeViewModel.Company = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Profileid equals d.Id
                       join f in _context.Company on d.Companyid equals f.Id
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select f).FirstOrDefault();
            enrolleeViewModel.CompanyPlan = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Profileid equals d.Id
                       join f in _context.Company on d.Companyid equals f.Id
                       join g in _context.CompanyPlan on d.Subscriptionplanid equals g.Id
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select g).FirstOrDefault();
            enrolleeViewModel.Provider = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Profileid equals d.Id
                       join h in _context.Providers on d.Primaryprovider equals h.Id
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select h).FirstOrDefault();
            enrolleeViewModel.EnrolleePassport = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Profileid equals d.Id
                       join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select e).FirstOrDefault();
            enrolleeViewModel.CompanySubsidiary = (
                       from c in _context.Staffs
                       join i in _context.CompanySubsidiary on c.CompanySubsidiary equals i.Id
                       where c.StaffId == user.StaffId && c.IsDeleted == false
                       select i).FirstOrDefault();

            enrolleeViewModel.State = (
                      from c in _context.Staffs
                      join d in _context.Enrollees on c.Profileid equals d.Id
                      join j in _context.States on d.Stateid equals j.Id
                      where c.StaffId == user.StaffId && c.IsDeleted == false
                      select j).FirstOrDefault();

            ViewBag.count = (from c in _context.Staffs
                             join d in _context.Enrollees on c.Profileid equals d.Parentid
                             where c.StaffId == user.StaffId && c.IsDeleted == false
                             select d).Count();

            ViewBag.enrolleeimg = Convert.ToBase64String(enrolleeViewModel.EnrolleePassport.Imgraw);
            TempData["img"] = Convert.ToBase64String(enrolleeViewModel.EnrolleePassport.Imgraw);
            TempData["coy"] = enrolleeViewModel.Company.Name;
            if(enrolleeViewModel.Enrollee == null)
            {
                TempData["Message"] = "Complete your registration before you can view this page";
                return RedirectToAction("NotRegistered", "Enrollees");
            }
            return View(enrolleeViewModel);
        }

      
        public async Task<IActionResult> Dependents(int Id)
        {
            ViewBag.Policyno = Id;
            var user = await _userManager.GetUserAsync(User);

            var dependants = _context.Enrollees.Where(x => x.Staffprofileid == Id && x.Parentid != 0).ToList();

            //var dependants = (from e in _context.Enrollees
            //                  where Policynumber.Contains(Policynumber) && e.IsDeleted == false
            //                  select e).ToList();

            //DependentViewModel dependentViewModel = new DependentViewModel();
            //var dependants = (from e in _context.Enrollees
            //                  where Policynumber.Contains(Policynumber) && e.IsDeleted == false
            //                  select e).ToList();


            //var EnrolleePassport = (
            //               from f in _context.EnrolleePassport
            //               where f.Enrolleepolicyno.Contains(Policynumber) && f.IsDeleted == false
            //               select f).ToList();

            //var Provider = (from e in _context.Enrollees
            //                               join f in _context.Providers on e.Primaryprovider equals f.Id
            //                               where e.Policynumber.Contains(Policynumber) && e.IsDeleted == false
            //                               select f).ToList();

            //ViewBag.count = (from c in _context.Staffs
            //                 join d in _context.Enrollees on c.Id equals d.Staffprofileid
            //                 where c.StaffId == user.StaffId && c.IsDeleted == false && d.Staffprofileid == Id
            //                 select d).Count();

            ViewBag.Dependants = dependants;

            if (ViewBag.Dependants == null)
            {
                return NotFound();
            }

            return View(ViewBag.Dependants);
        }

        public async Task<IActionResult> IdCards(int? Id)
        {

            var user = await _userManager.GetUserAsync(User);

            EnrolleeViewModel enrolleeViewModel = new EnrolleeViewModel();
            enrolleeViewModel.Enrollee = (
                         from c in _context.Staffs
                         join d in _context.Enrollees on c.Id equals d.Staffprofileid
                         join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                         where d.Id == Id && c.IsDeleted == false
                         select d).FirstOrDefault();

            enrolleeViewModel.Staff = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       where d.Id == Id && c.IsDeleted == false
                       select c).FirstOrDefault();
            enrolleeViewModel.Company = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join f in _context.Company on d.Companyid equals f.Id
                       where d.Id == Id && c.IsDeleted == false
                       select f).FirstOrDefault();
            enrolleeViewModel.CompanyPlan = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join f in _context.Company on d.Companyid equals f.Id
                       join g in _context.CompanyPlan on d.Subscriptionplanid equals g.Id
                       where d.Id == Id && c.IsDeleted == false
                       select g).FirstOrDefault();
            enrolleeViewModel.Provider = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join h in _context.Providers on d.Primaryprovider equals h.Id
                       where d.Id == Id && c.IsDeleted == false
                       select h).FirstOrDefault();
            enrolleeViewModel.EnrolleePassport = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                       where d.Id == Id && c.IsDeleted == false
                       select e).FirstOrDefault();
            enrolleeViewModel.CompanySubsidiary = (
                       from c in _context.Enrollees
                       join d in _context.Staffs on c.Staffprofileid equals d.Id
                       join i in _context.CompanySubsidiary on d.CompanySubsidiary equals i.Id
                       where d.Id == Id && c.IsDeleted == false
                       select i).FirstOrDefault();

            enrolleeViewModel.State = (
                      from c in _context.Staffs
                      join d in _context.Enrollees on c.Id equals d.Staffprofileid
                      join j in _context.States on d.Stateid equals j.Id
                      where d.Id == Id && c.IsDeleted == false
                      select j).FirstOrDefault();

            if (enrolleeViewModel.EnrolleePassport == null)
            {
                return Content("I can not display this Id Card because no Passport Photograph attached to the Profile");
            }


            ViewBag.enrolleeimg = Convert.ToBase64String(enrolleeViewModel.EnrolleePassport.Imgraw);
            return View(enrolleeViewModel);
        }

        public IActionResult NotRegistered()
        {
            return View();
        }

            public async Task<IActionResult> DepenCards(int? Id)
        {

            var user = await _userManager.GetUserAsync(User);

            EnrolleeViewModel enrolleeViewModel = new EnrolleeViewModel();
            enrolleeViewModel.Enrollee = (
                         from c in _context.Staffs
                         join d in _context.Enrollees on c.Id equals d.Staffprofileid
                         join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                         where d.Id == Id && c.IsDeleted == false
                         select d).FirstOrDefault();

            enrolleeViewModel.Staff = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       where d.Id == Id && c.IsDeleted == false
                       select c).FirstOrDefault();
            enrolleeViewModel.Company = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join f in _context.Company on d.Companyid equals f.Id
                       where d.Id == Id && c.IsDeleted == false
                       select f).FirstOrDefault();
            enrolleeViewModel.CompanyPlan = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join f in _context.Company on d.Companyid equals f.Id
                       join g in _context.CompanyPlan on d.Subscriptionplanid equals g.Id
                       where d.Id == Id && c.IsDeleted == false
                       select g).FirstOrDefault();
            enrolleeViewModel.Provider = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join h in _context.Providers on d.Primaryprovider equals h.Id
                       where d.Id == Id && c.IsDeleted == false
                       select h).FirstOrDefault();
            enrolleeViewModel.EnrolleePassport = (
                       from c in _context.Staffs
                       join d in _context.Enrollees on c.Id equals d.Staffprofileid
                       join e in _context.EnrolleePassport on d.Policynumber equals e.Enrolleepolicyno
                       where d.Id == Id && c.IsDeleted == false
                       select e).FirstOrDefault();
            enrolleeViewModel.CompanySubsidiary = (
                       from c in _context.Enrollees
                       join d in _context.Staffs on c.Staffprofileid equals d.Id
                       join i in _context.CompanySubsidiary on d.CompanySubsidiary equals i.Id
                       where d.Id == Id && c.IsDeleted == false
                       select i).FirstOrDefault();

            enrolleeViewModel.State = (
                      from c in _context.Staffs
                      join d in _context.Enrollees on c.Id equals d.Staffprofileid
                      join j in _context.States on d.Stateid equals j.Id
                      where d.Id == Id && c.IsDeleted == false
                      select j).FirstOrDefault();

            if (enrolleeViewModel.EnrolleePassport == null)
            {
                return Content("I can not display this Id Card because no Passport Photograph attached to the Profile");
            }


            ViewBag.enrolleeimg = Convert.ToBase64String(enrolleeViewModel.EnrolleePassport.Imgraw);
            return View(enrolleeViewModel);
        }


        public IActionResult NewEnrollee()
        {
            //provides suggestions while you type into the field
            var name = HttpContext.Request.Query["term"].ToString();
            var StaffId = _context.Staffs.Where(c => c.StaffId.Contains(name)).Select(c => c.StaffId).ToList();
            return Ok(StaffId);
        }
        [HttpGet]
        public JsonResult NewEnrollee2(string name)
        {
            //fill input fields when you select CNPJ CLIENTE
            var StaffFullname = _context.Staffs.Where(c => c.StaffId == name).Select(c => c.StaffFullname).ToList();
            return new JsonResult(StaffFullname);
        }


        public IActionResult Create(int? id, string StaffId, string StaffFullname, string Company)
        {

            TempData["StaffName"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffFullname).FirstOrDefault();
            TempData["Staffplanid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            TempData["Staffprofileid"] = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.Id).FirstOrDefault();
            var staffplanid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            var Planid = _context.CompanyPlan.Where(x => x.Id == staffplanid).Select(x => x.Planid).FirstOrDefault();

            ViewBag.State = new SelectList(_context.States.Where(x => x.IsDeleted == false).OrderBy(x => x.Name), "Id", "Name");
            ViewData["Stateid"] = new SelectList(_context.States.Where(x => x.IsDeleted == false).OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())).OrderBy(x => x.Name), "Id", "Name");
            ViewData["Providerid"] = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())).OrderBy(x => x.Name), "Id", "Name"); ;

            ViewBag.Lga = new SelectList(_context.Lgas, "Id", "Name");
            var Lgalist = new SelectList(_context.Lgas, "Id", "Name");



            ViewBag.Companyid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.CompanyId).FirstOrDefault();

            return View();
        }

        // POST: EnProfile/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Parentid,Parentrelationship,Policynumber,Title,Surname,Othernames,Dob,Age,Maritalstatus,Occupation,Sex,Residentialaddress,Stateid,Lgaid,Mobilenumber,Emailaddress,Sponsorshiptype,Sponsorshiptypeothername,Preexistingmedicalhistory,Sponsorshiptypenote,Companyid,Subscriptionplanid,Hasdependents,Specialidcardfield1,Specialidcardfield2,Specialidcardfield3,Staffprofileid,Primaryprovider,Status,Hasactivesubscription,Isexpundged,ExpungeNote,Expungedby,Dateexpunged,Createdby,Datereceived,Guid,CreatedOn,UpdatedOn,IsDeleted,EnrolleePassportId,IdCardPrinted,RefPolicynumber,HasRefPolicyNumber,Mobilenumber2,LastyearBirthdaymsgsent,Bulkjobid,Passphrase")] Enrollee enrollee, string StaffId)
        {

            var staffplanid = _context.Staffs.Where(x => x.StaffId == StaffId).Select(x => x.StaffPlanid).FirstOrDefault();
            var Planid = _context.CompanyPlan.Where(x => x.Id == staffplanid).Select(x => x.Planid).FirstOrDefault();

            ViewBag.State = new SelectList(_context.States.Where(x => x.IsDeleted == false).OrderBy(x => x.Name), "Id", "Name");
            ViewData["Stateid"] = new SelectList(_context.States.Where(x => x.IsDeleted == false).OrderBy(x => x.Name), "Id", "Name");
            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())).OrderBy(x => x.Name), "Id", "Name");
            ViewData["Providerid"] = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(Planid.ToString())).OrderBy(x => x.Name), "Id", "Name"); ;

            ViewBag.Lga = new SelectList(_context.Lgas, "Id", "Name");
            var Lgalist = new SelectList(_context.Lgas, "Id", "Name");
            if (ModelState.IsValid)
            {
                _context.Add(enrollee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(enrollee);
        }

    }
}
