﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public TestController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        public async Task<IActionResult> Test()
        {
            var user = await _userManager.GetUserAsync(User);
            //var staffid = ((await _userManager.GetUserAsync(User)).StaffId);


            var Enrollee = (from a in _context.Staffs
                            join b in _context.Enrollees on a.Profileid equals b.Id
                            join c in _context.Company on b.Companyid equals c.Id
                            join d in _context.Providers on b.Primaryprovider equals d.Id
                            where a.StaffId == user.StaffId
                            select new
                            {
                                MyStaffId = a.StaffId,
                                b,
                                c,
                                d
                            }).FirstOrDefault();

            var CompanyBenefit = (from e in _context.Staffs
                                  join h in _context.CompanyBenefit on e.StaffPlanid  equals h.CompanyPlanid
                                  join i in _context.Benefits on h.BenefitId equals i.Id
                                  where e.StaffId == user.StaffId
                                  && h.IsDeleted == false && e.IsDeleted == false && e.IsExpunged == false
                                  select new { 
                                      BenefitName = i.Name,
                                      BenefitDescription = i.Description,
                                      Limit = h.BenefitLimit
                                  }).ToList();

            var AppointmentViewModel = new /*AppointmentViewModel*/
            {

                Enrollees = Enrollee,
                CompanyBenefits = CompanyBenefit

            };

            return Ok(AppointmentViewModel);
        }


        //public async Task<IActionResult> Dependents()
        //{

        //    var staff = (from a in _context.Staffs
        //                 join b in _context.Enrollees on a.Profileid equals b.Parentid
        //                 join c in _context.Providers on b.Primaryprovider equals c.Id
        //                 join d in _context.States on c.StateId equals d.Id
        //                 select new
        //                 {
        //                     PryEnrollee = a.StaffFullname,
        //                     FName = b.Surname + " " + b.Othernames,
        //                     Policyno = b.Policynumber,
        //                     PhoneNo = b.Mobilenumber,
        //                     EmailId = b.Emailaddress,
        //                     Gender = b.Sex,
        //                     Relationship = b.Parentrelationship,
        //                     ProviderName = c.Name,
        //                     ProviderLocation = c.Address + ", " + d.Name

        //                 }).ToList();

        //    var model = new /*AppointmentViewModel*/
        //    {

                
        //        Staff = staff

        //    };

        //    return Ok(model);
        //}

    }
}