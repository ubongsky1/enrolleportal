﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DependantsController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public DependantsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

       
        // GET: Dependents
        public async Task<IActionResult> Dependents()
        {
            var user = await _userManager.GetUserAsync(User);
            var enrollees = (from a in _context.Staffs
                             join b in _context.Enrollees on a.Profileid equals b.Parentid
                             join c in _context.Providers on b.Primaryprovider equals c.Id
                             join d in _context.States on c.StateId equals d.Id
                             where a.StaffId == user.StaffId && a.IsDeleted == false && b.Isexpundged == false
                             && b.IsDeleted == false
                             select new
                             {
                                 Fullname = a.StaffFullname,
                                 DependentName = b.Surname + " " + b.Othernames,
                                 DependentPhone = b.Mobilenumber,
                                 DependentEmail = b.Emailaddress,
                                 PryProvider = c.Name,
                                 ProviderState = d.Name,
                                 DependentDob = b.Dob,
                                 DependentGender = b.Sex,
                                 Relationship = b.Parentrelationship
                             }).ToList();
            return Ok(enrollees);
        }

    }
}