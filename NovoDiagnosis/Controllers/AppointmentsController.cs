﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoDiagnosis.Models.ViewModels;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class AppointmentsController : Controller
    {
       
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AppointmentsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

       

        // GET: Appointments
        public async Task<IActionResult> Index(Appointment appoinment, Staff staff, Provider provider)
        {
            var user = await _userManager.GetUserAsync(User);

            var Planid = _context.Staffs.Where(x => x.StaffId == user.StaffId)
                          .Select(x => x.StaffPlanid).FirstOrDefault();

            var id = _context.CompanyPlan.Where(x => x.Id == Planid).Select(x => x.Planid).FirstOrDefault();



            ViewBag.Provider = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(id.ToString())), "Id", "Name");
            var Providerlist = new SelectList(_context.Providers.Where(x => x.isDelisted == false && x.Providerplans.Contains(id.ToString())), "Id", "Name", appoinment.ProviderId);

            ViewBag.Profileid = _context.Staffs.Where(x => x.StaffId == user.StaffId && x.IsDeleted == false).Select(x => x.Profileid);

            ViewData["ProviderList"] = Providerlist;

            var applicationDbContext = _context.Appointments.Include(a => a.Provider);
            return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult>BenefitList()
        {
            var user = await _userManager.GetUserAsync(User);
            

            return View();
        }


        //public async Task<IActionResult> Dependents()
        //{

        //    var staff = (from a in _context.Staffs
        //                          join b in _context.Enrollees on a.Profileid equals b.Parentid
        //                          join c in _context.Providers on b.Primaryprovider equals c.Id
        //                          join d in _context.States on c.StateId equals d.Id
        //                          select new
        //                          {
        //                              PryEnrollee = a.StaffFullname,
        //                              FName = b.Surname + " " + b.Othernames,
        //                              Policyno = b.Policynumber,
        //                              PhoneNo = b.Mobilenumber,
        //                              EmailId = b.Emailaddress,
        //                              Gender = b.Sex,
        //                              Relationship = b.Parentrelationship,
        //                              ProviderName = c.Name,
        //                              ProviderLocation = c.Address + ", " + d.Name

        //                          });

        //    //var model = new AppointmentViewModel { Staffs = staff };
           
        //    return View(staff);
        //}
        // GET: Appointments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointments
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appointment == null)
            {
                return NotFound();
            }

            return View(appointment);
        }

        // GET: Appointments1/Create
        public IActionResult Create()
        {
            ViewData["ProviderId"] = new SelectList(_context.Providers, "Id", "Id");
            return View();
        }

        // POST: Appointments1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FullName,StaffId,Policynumber,Complain,Symptoms,Otherdetails,VerificationCode,CallCenterRemark,ProviderRemark,ProviderId,State,Email,PhoneNumber,AppointmentDate,AppointmentTime,CompletedAnnualMedical,Approve,Reject,CreatedOn,UpdatedOn,AuthorizationCode,Recommendation,UserName,Status,History,PresentingComplain,Diagnosis,PlanTest,UserReview")] Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(appointment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProviderId"] = new SelectList(_context.Providers, "Id", "Id", appointment.ProviderId);
            return View(appointment);
        }

        // GET: Appointments1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointments.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }
            ViewData["ProviderId"] = new SelectList(_context.Providers, "Id", "Id", appointment.ProviderId);
            return View(appointment);
        }

        // POST: Appointments1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FullName,StaffId,Policynumber,Complain,Symptoms,Otherdetails,VerificationCode,CallCenterRemark,ProviderRemark,ProviderId,State,Email,PhoneNumber,AppointmentDate,AppointmentTime,CompletedAnnualMedical,Approve,Reject,CreatedOn,UpdatedOn,AuthorizationCode,Recommendation,UserName,Status,History,PresentingComplain,Diagnosis,PlanTest,UserReview")] Appointment appointment)
        {
            if (id != appointment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(appointment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppointmentExists(appointment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProviderId"] = new SelectList(_context.Providers, "Id", "Id", appointment.ProviderId);
            return View(appointment);
        }

        // GET: Appointments1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointments
                .Include(a => a.Provider)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appointment == null)
            {
                return NotFound();
            }

            return View(appointment);
        }

        // POST: Appointments1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appointment = await _context.Appointments.FindAsync(id);
            _context.Appointments.Remove(appointment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AppointmentExists(int id)
        {
            return _context.Appointments.Any(e => e.Id == id);
        }
    }
}
