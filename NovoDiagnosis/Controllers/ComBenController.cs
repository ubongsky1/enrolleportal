﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComBenController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ComBenController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        public async Task<IActionResult> CompanyBenefits()
        {

            var user = await _userManager.GetUserAsync(User);
            var CompanyBenefits = (from d in _context.Staffs
                                   join i in _context.CompanyBenefit on d.StaffPlanid equals i.CompanyPlanid
                                   join j in _context.CompanyPlan on i.CompanyPlanid equals j.Planid
                                   join o in _context.Enrollees on d.Profileid equals o.Id
                                   where d.StaffId == user.StaffId && d.IsDeleted == false
                                   select new
                                   {

                                       FullName = d.StaffFullname,
                                       Benefit = i.BenefitLimit,
                                       Planname = j.Planfriendlyname,
                                       Noofdependant = j.MaxNoOfDependant,
                                       benefitdescription = j.Description,
                                       PolicyNo = o.Policynumber,
                                       MobileNo = o.Mobilenumber,
                                       MobileNo2 = o.Mobilenumber2,
                                       status = j.Status,
                                       createdOn = i.CreatedOn

                                   });

            return Ok(CompanyBenefits.ToList());




        }
    }
}