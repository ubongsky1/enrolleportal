﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoEnrollee.Data;
using NovoEnrollee.Models;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Security.Cryptography;
using NovoDiagnosis.Models;

namespace NovoEnrollee.Controllers
{

    [Authorize]
    public class DiagnosisDetailsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private static Random random = new Random();


        public DiagnosisDetailsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public async Task<IActionResult> First()
        {
            var user = await _userManager.GetUserAsync(User);
           
            return View();
        }
        public ActionResult Enrollee()
        {

            var policynumber = TempData.Peek("policyno")?.ToString();

            return View(policynumber);
        }
        // GET: DiagnosisDetails2
        public async Task<IActionResult> Index()
        {

            var user = await _userManager.GetUserAsync(User);
           
                return View();
           

        }

        // GET: DiagnosisDetails2
        public async Task<IActionResult> AppointmentList()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user.PasswordHash == "AQAAAAEAACcQAAAAEN2HalQQodr2Hf7ezR2G9YeFGbOlfpEAzSlEjmrdVYQ2E4BjmBc0+z7oubzza8zclQ==" && user.Email != "No Access to Mail")
            {
                var returnurl = "https://demonovodiag.azurewebsites.net/Identity/Account/ForgotPassword";

                return Redirect(returnurl);
                //return View();
            }
            else
            {
                return View(await _context.DiagnosisDetails.ToListAsync());
            }
                
        }

        public async Task<IActionResult> MyAppointment()
        {
            var user = await _userManager.GetUserAsync(User);
            var myappointment = _context.DiagnosisDetails.Where(x => x.UserName == user.UserName);
            ViewBag.UserName = myappointment;
            if (user.PasswordHash == "AQAAAAEAACcQAAAAEN2HalQQodr2Hf7ezR2G9YeFGbOlfpEAzSlEjmrdVYQ2E4BjmBc0+z7oubzza8zclQ==" && user.Email != "No Access to Mail")
            {
                var returnurl = "https://demonovodiag.azurewebsites.net/Identity/Account/ForgotPassword";

                return Redirect(returnurl);
                //return View();
            }
            else {
                if (myappointment == null)
                {
                    return RedirectToAction("Details", "DiagnosisDetails");
                }
                return View(myappointment);
            }
            
        }

        // GET: DiagnosisDetails2/Details/5
        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);

            var diagnosisDetails = await _context.DiagnosisDetails
                .LastOrDefaultAsync(m => m.StaffId == user.UserName);
            if (user.PasswordHash == "AQAAAAEAACcQAAAAEN2HalQQodr2Hf7ezR2G9YeFGbOlfpEAzSlEjmrdVYQ2E4BjmBc0+z7oubzza8zclQ==" && user.Email != "No Access to Mail")
            {
                var returnurl = "https://demonovodiag.azurewebsites.net/Identity/Account/ForgotPassword";

                return Redirect(returnurl);
                //return View();
            }
            else
            {
                if (diagnosisDetails != null)
                {
                    return View(diagnosisDetails);
                }

                ViewBag.Error = "You currently have no appointment, Kindly create one.";
                return RedirectToAction("Create", "DiagnosisDetails");
            }


        }


        public async Task<IActionResult> Referral()
        {
            var user = await _userManager.GetUserAsync(User);

            var diagnosisDetails = await _context.DiagnosisDetails
                .LastOrDefaultAsync(m => m.StaffId == user.UserName);
            if (user.PasswordHash == "AQAAAAEAACcQAAAAEN2HalQQodr2Hf7ezR2G9YeFGbOlfpEAzSlEjmrdVYQ2E4BjmBc0+z7oubzza8zclQ==" && user.Email != "No Access to Mail")
            {
                var returnurl = "https://demonovodiag.azurewebsites.net/Identity/Account/ForgotPassword";

                return Redirect(returnurl);
                //return View();
            }
            else
            {
                if (diagnosisDetails != null)
                {
                    return View(diagnosisDetails);
                }

                ViewBag.Error = "You currently have no appointment, Kindly create one.";
                return RedirectToAction("Create", "DiagnosisDetails");
            }


        }
        public IActionResult NoRecordFound()
        {
            return View();
        }

        // GET: DiagnosisDetails2/Create
        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Profile()
        {

            return View();
        }

        public IActionResult BenefitTable()
        {

            return View();
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( DiagnosisDetails diagnosisDetails, string Email, string AppointmentDate, string Provider, string AuthorizationCode)
        {

           
            var user = await _userManager.GetUserAsync(User);
            if (ModelState.IsValid)
            {
                _context.Add(diagnosisDetails);
                await _context.SaveChangesAsync();

                try
                {
                    //instantiate a new MimeMessage
                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(Email));

                    message.Bcc.Add(new MailboxAddress("callcenter@novohealthafrica.org"));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Name", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "Appointment Successfully Created";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='~/img/logo.png' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                       "<p>Dear " + user.FullName + ",</p><br/>" +

                        "Kindly be informed that your annual medical checkup has been Scheduled at <b>" + Provider + 
                        " on " +  AppointmentDate + "." +
                        "</b>.<br/><br/> Your authorization code is <b>" + AuthorizationCode + "</b>." +
                        "<br/> Below are the panel of investigations to be conducted:<br /><br />" +
                        "<ul>" +

                                                      "<li> Routine Clinic Examinations </li>" +

                                                      "<li> Physical Examination </li>" +

                                                      "<li> Urinalysis </li>" +

                                                      "<li> Hepatitis B(HBaAg) </li>" +

                                                      "<li> PCV, Fasting Blood Sugar </li>" +

                                                      "<li> ECG </li>" +

                                                      "<li> E / U / CR(Kidney Function Test) </li>" +

                                                      "<li> Liver Function Test </li>" +

                                                      "<li> Lipid Profile </li>" +

                                                 " </ul>" +


                                                  "<br/>" +

                                                  "<br/> Thank you for choosing Novo Health Africa." +

                                                  "<br/><br/>" +

                                                   //"<img src = '~/images/letterheadfoot.png' width = '650' height = '100' >"
                                                   "<hr width='60%'/>" +
                                                   "<p align = 'center'><small> Lagos: Ploat 10A, Akiogun Road, Oniru, Lagos State.</small><br />" +
                                                "<p align='center'><small>Abuja: Block B, Ground Floor,Plot 564 / 565 Independence Avenue,AUJ Complex, Central Business District, F.C.T., Abuja.</small><br/>" +
                                                "<p align='center'><small>Kano: Suite C6, Ground Floor, Ummi Plza, 14 / 15 Trade Fair Complex.Off Zaria Road,  Kano State.</small><br/>" +
                                                "<p align='center'><small>Port - Harcourt: 2nd Floor, BSIC 15 Mall, 66 Olu Obasanjo Way, Port - Harcourt, Rivers State.</small><br/>" +
                                                "<p align ='center'><small>Enugu: 123,  Chime Avenue.Zandans Plaza, New Heaven, Enugu State.</small><br/>" +
                                               "<p align='center'><small> Ibadan: Suite 7,ASE Complex, Before State Veterinary Junction,Mokola Ibadan.</small><br />" +
                                               "<p align='center'><small> Tel: +234(1) 2900047 Email: info@novohealthafrica.org Website: www.novohealthafrica.org</small><br/>"


                        //Text = "<p>Dear " + user.FullName + ",</p><br/>" +
                        //"<p>Thank you for creating a new appointment using the Novo Annual Medical Check App.</p>" +
                        //"<p>Your annual medical checkup have been scheduled to take place at <b>" + Provider + "</b> at <b>" + AppointmentDate + "</b>. </p><p></p><p>Your Authorization Code is <b>" + AuthorizationCode + "</b>.<br/> Kindly go to the Hospital with this code.</p>" +
                        //"<p>You have been authorized for the following Medical Checks: </p>" +
                        //"<ul><li>Routine Clinic Examinations</li><li>Physical Examination</li><li>Urinalysis</li><li>Hepatitis B (HBaAg)</li><li>PCV, Fasting Blood Sugar</li><li>ECG</li><li>E/U/CR (Kidney Function Test)</li><li>Liver Function Test</li><li>Lipid Profile</li></ul>" +


                        //"<br/><br><br/><p>Thank you for choosing Novo Health Africa</p>"
                    };
                
                    //Configure the e-mail
                    using (var emailClient = new SmtpClient())
                    {
                        emailClient.Connect("smtp.office365.com", 587, false);
                        emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.Clear();
                    ViewBag.Message = $" Oops! We have a problem here {ex.Message}";
                }
                return RedirectToAction("Details", "DiagnosisDetails");
            }
           
            
            return View(diagnosisDetails);
        }

        // GET: DiagnosisDetails2/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisDetails = await _context.DiagnosisDetails.FindAsync(id);
            if (diagnosisDetails == null)
            {
                return NotFound();
            }
            return View(diagnosisDetails);
        }

        // POST: DiagnosisDetails2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FullName,StaffId,Provider,State,Email,PhoneNumber,AppointmentDate,AppointmentTime,CompletedAnnualMedical,Approve,Reject,CreatedOn,UpdatedOn,AuthorizationCode,Recommendation,UserName,Status,History,PresentingComplain,Diagnosis,PlanTest")] DiagnosisDetails diagnosisDetails)
        {
            if (id != diagnosisDetails.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(diagnosisDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiagnosisDetailsExists(diagnosisDetails.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "DiagnosisDetails");
            }
            return View(diagnosisDetails);
        }

        // GET: DiagnosisDetails2/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var diagnosisDetails = await _context.DiagnosisDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (diagnosisDetails == null)
            {
                return NotFound();
            }

            return View(diagnosisDetails);
        }

        // POST: DiagnosisDetails2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var diagnosisDetails = await _context.DiagnosisDetails.FindAsync(id);
            _context.DiagnosisDetails.Remove(diagnosisDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DiagnosisDetailsExists(int id)
        {
            return _context.DiagnosisDetails.Any(e => e.Id == id);
        }
    }
}
