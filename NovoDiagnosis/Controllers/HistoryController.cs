﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;
using NovoEnrollee.Models;
using Claim = NovoEnrollee.Models.Claim;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public HistoryController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // GET: MedicalHistory
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

           

            //ViewData["MedicalHistory"] = history;

            return Ok(/*await applicationDbContext.ToListAsync()*/);
        }

    }
}