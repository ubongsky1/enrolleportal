﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class ProvidersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProvidersController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: Providers
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            //var PlanId = _context.Staffs.Where(x => x.StaffId == user.StaffId).Select(x => x.StaffId).FirstOrDefault();
            ////String s;
            //var num = PlanId.ToString();
            ////s = num.ToString();
            var Provider = (
                            from d in _context.Providers
                            from a in _context.Staffs
                            join e in _context.States on d.StateId equals e.Id
                            join f in _context.CompanyPlan on a.StaffPlanid equals f.Id
                            where a.StaffId == user.StaffId && d.isDelisted == false
                            && d.IsDeleted == false && d.Providerplans.Contains(f.Planid.ToString())
                            select new Provider
                            {
                                Name = d.Name,
                                Address = d.Address,
                                Area = d.Area,
                                State = e

                            }).Distinct();

            ViewData["Providers"] = Provider;
            //var applicationDbContext = _context.Providers.Include(p => p.State);
            return View();
        }

        // GET: Providers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .Include(p => p.State)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // GET: Providers/Create
        public IActionResult Create()
        {
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Id");
            return View();
        }

        // POST: Providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Code,SubCode,Email,Phone,Phone2,Website,Address,Area,StateId,PaymentEmail1,PaymentEmail2,Assignee,Providergpscordinate,Providerservices,Providerplans,ProviderTariffs,CreatedBy,AuthorizationStatus,AuthorizationNote,DisapprovalNote,AuthorizedBy,DisapprovedBy,AuthorizedDate,DisapprovalDate,DeletionNote,Parentid,Status,isDelisted,DelistNote,delisteddate,delistedBy,CompanyConsession")] Provider provider)
        {
            if (ModelState.IsValid)
            {
                _context.Add(provider);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Id", provider.StateId);
            return View(provider);
        }

        // GET: Providers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers.FindAsync(id);
            if (provider == null)
            {
                return NotFound();
            }
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Id", provider.StateId);
            return View(provider);
        }

        // POST: Providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Code,SubCode,Email,Phone,Phone2,Website,Address,Area,StateId,PaymentEmail1,PaymentEmail2,Assignee,Providergpscordinate,Providerservices,Providerplans,ProviderTariffs,CreatedBy,AuthorizationStatus,AuthorizationNote,DisapprovalNote,AuthorizedBy,DisapprovedBy,AuthorizedDate,DisapprovalDate,DeletionNote,Parentid,Status,isDelisted,DelistNote,delisteddate,delistedBy,CompanyConsession")] Provider provider)
        {
            if (id != provider.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(provider);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProviderExists(provider.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Id", provider.StateId);
            return View(provider);
        }

        // GET: Providers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provider = await _context.Providers
                .Include(p => p.State)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (provider == null)
            {
                return NotFound();
            }

            return View(provider);
        }

        // POST: Providers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var provider = await _context.Providers.FindAsync(id);
            _context.Providers.Remove(provider);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProviderExists(int id)
        {
            return _context.Providers.Any(e => e.Id == id);
        }
    }
}
