﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;
using NovoEnrollee.Models;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BenefitsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public BenefitsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: api/Benefits
        [HttpGet]
        public async Task<IActionResult> GetBenefits()
        {
            var user =  await _userManager.GetUserAsync(User);
            var benefit = (from e in _context.Staffs
                           join h in _context.CompanyBenefit on e.StaffPlanid equals h.CompanyPlanid
                           join i in _context.Benefits on h.BenefitId equals i.Id
                           where e.StaffId == user.StaffId
                           && h.IsDeleted == false 
                           select new
                           {
                               name = i.Name,
                               description = i.Description,
                               benefitlimit =  h.BenefitLimit + ", " + i.Benefitlimit

                           }).ToList();

            return Ok(benefit);
        }

        // GET: api/Benefits/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBenefit()
        {
            var user = await _userManager.GetUserAsync(User);
            var benefit = (from e in _context.Staffs
            join h in _context.CompanyBenefit on e.StaffPlanid equals h.CompanyPlanid
            join i in _context.Benefits on h.BenefitId equals i.Id
            where e.StaffId == user.StaffId
            && h.IsDeleted == false && e.IsDeleted == false && e.IsExpunged == false
            select new
            {
                BenefitName = i.Name,
                BenefitDescription = i.Description,
                Limit = h.BenefitLimit + ", " + i.Benefitlimit
                
            }).ToList();

            return Ok(benefit);
        }

        // PUT: api/Benefits/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBenefit([FromRoute] int id, [FromBody] Benefit benefit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != benefit.Id)
            {
                return BadRequest();
            }

            _context.Entry(benefit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BenefitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Benefits
        [HttpPost]
        public async Task<IActionResult> PostBenefit([FromBody] Benefit benefit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Benefits.Add(benefit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBenefit", new { id = benefit.Id }, benefit);
        }

        // DELETE: api/Benefits/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBenefit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var benefit = await _context.Benefits.FindAsync(id);
            if (benefit == null)
            {
                return NotFound();
            }

            _context.Benefits.Remove(benefit);
            await _context.SaveChangesAsync();

            return Ok(benefit);
        }

        private bool BenefitExists(int id)
        {
            return _context.Benefits.Any(e => e.Id == id);
        }
    }
}