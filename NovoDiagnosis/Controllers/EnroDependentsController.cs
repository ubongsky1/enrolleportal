﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnroDependentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public EnroDependentsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        //GET: api/EnroDependents
        [HttpGet]
        public async Task<IActionResult> GetEnrollees()
        {
            var user = await _userManager.GetUserAsync(User);
            var Enrollee = (from c in _context.Staffs
                            join d in _context.Enrollees on c.Profileid equals d.Id
                            join e in _context.Enrollees on d.Id equals e.Parentid
                            join f in _context.Providers on d.Primaryprovider equals f.Id
                            where c.StaffId == user.StaffId && c.IsDeleted == false
                            && e.Policynumber.Contains(d.Policynumber)
                            select new
                            {
                                //MyStaffId = a.StaffId,
                                id = e.Id,
                                name = e.Surname + " " + e.Othernames,
                                policyno = e.Policynumber,
                                relationship = e.Parentrelationship,
                                providerofchoice = f.Name
                                //c,
                                //d
                            }).ToList();
            if (Enrollee == null)
            {
                return Content("Y");
            }
            return Ok(Enrollee);
        }

        // GET: api/EnroDependents/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEnrollee()
        {
            var user = await _userManager.GetUserAsync(User);
            var Enrollee = (from a in _context.Staffs
                            join b in _context.Enrollees on a.Profileid equals b.Parentid
                            //join c in _context.Company on b.Companyid equals c.Id
                            //join d in _context.Providers on b.Primaryprovider equals d.Id
                            where a.StaffId == user.StaffId && a.IsDeleted == false && b.IsDeleted == false
                            select new
                            {
                                //MyStaffId = a.StaffId,
                                b,
                                //c,
                                //d
                            }).ToList();

            return Ok(Enrollee);


        }

        // PUT: api/EnroDependents/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEnrollee([FromRoute] int id, [FromBody] Enrollee enrollee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != enrollee.Id)
            {
                return BadRequest();
            }

            _context.Entry(enrollee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnrolleeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EnroDependents
        [HttpPost]
        public async Task<IActionResult> PostEnrollee([FromBody] Enrollee enrollee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Enrollees.Add(enrollee);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEnrollee", new { id = enrollee.Id }, enrollee);
        }

        // DELETE: api/EnroDependents/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEnrollee([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var enrollee = await _context.Enrollees.FindAsync(id);
            if (enrollee == null)
            {
                return NotFound();
            }

            _context.Enrollees.Remove(enrollee);
            await _context.SaveChangesAsync();

            return Ok(enrollee);
        }

        private bool EnrolleeExists(int id)
        {
            return _context.Enrollees.Any(e => e.Id == id);
        }
    }
}