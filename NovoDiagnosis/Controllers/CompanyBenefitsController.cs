﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NovoDiagnosis.Models;
using NovoDiagnosis.Models.ViewModels;
using NovoEnrollee.Data;
using NovoEnrollee.Models;

namespace NovoEnrollee.Controllers
{
    public class CompanyBenefitsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public CompanyBenefitsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        
        public async Task<IActionResult> Index()
        {

           var staffid =  ((await _userManager.GetUserAsync(User)).StaffId);


            var enrollee = (from a in _context.Staffs
                            join b in _context.Enrollees on a.Profileid equals b.Id
                            where a.StaffId == staffid
                            select new { a, b}).ToList();


            //var StaffId = _userManager.GetUserAsync(User).Result.StaffId;
            //var user = _userManager.GetUserAsync(User);



            //int Notifications = 0;
            //int Appointments = 0;
            //int Visithospital = 0;
            //var previousDate = DateTime.Now.AddMonths(-1);
            //var date = new DateTime(previousDate.Year, previousDate.Month, DateTime.Now.Day);


            //if (_context.Claims.Where(x => x.EnrolleePolicyNumber.Contains(PolicyNumber) || x.EnrolleeFullname == user.).Count() > 0)
            //{
            //    Visithospital = _context.Claims.Where(x => x.EnrolleePolicyNumber.Contains(PolicyNumber) || x.EnrolleeFullname == user.FullName).Count();
            //}
            //if (_context.Notifications.Where(x => x.PolicyNumber.Contains(PolicyNumber) || x.StaffId == user.StaffId).Count() > 0)
            //{
            //    Notifications = _context.Notifications.Where(x => x.PolicyNumber.Contains(PolicyNumber) || x.StaffId == user.StaffId).Count();
            //}
            //if (_context.Appointments.Where(x => x.Policynumber.Contains(PolicyNumber) || x.StaffId == user.StaffId).Count() > 0)

            //    Appointments = _context.Appointments.Where(x => x.Policynumber.Contains(PolicyNumber) || x.StaffId == user.StaffId).Count();

            ////return View(CompanyBenefits.ToList());


            //var benefit = new BenefitModel
            //{
            //    Enrollees = _context.Enrollees.Where(x => x.Policynumber.Contains(PolicyNumber)).Count(),
            //    Appointments = _context.Appointments.Where(x => x.Policynumber.Contains(PolicyNumber) || x.StaffId.Contains(user.StaffId)).Count(),
            //    Notifications = _context.Notifications.Where(x => x.PolicyNumber.Contains(PolicyNumber) || x.StaffId.Contains(user.StaffId)).Count(),

            //    MyCompanyPlans = _context.CompanyPlans.Where(x => x.Companyid == CompanyId),
            //    MyCompanyBenefits = _context.CompanyBenefit.Where(x => x.CompanyPlanid == StaffPlanId),
            //    MyNotifications = _context.Notifications.Where(x => x.StaffId == user.StaffId),
            //    MyProviders = _context.Providers,
            //    MyClaims = _context.Claims.Where(x => x.EnrolleeFullname == PolicyNumber || x.EnrolleeFullname == user.FullName)


            //};
            return View();

            //return View(await _context.CompanyBenefit.ToListAsync());
        }

        // GET: CompanyBenefits
        public async Task<IActionResult> CompanyBenefits()
        {

            var user = await _userManager.GetUserAsync(User);
            var CompanyBenefits = (from i in _context.CompanyBenefit
                              join j in _context.Benefits on i.BenefitId equals j.Id
                              join m in _context.Company on i.Companyid equals m.Id
                              join l in _context.Staffs on i.CompanyPlanid equals l.StaffPlanid
                              join k in _context.Enrollees on l.Id equals k.Staffprofileid
                              //join n in _context.CompanyPlans on l.StaffPlanid equals n.Planid
                              where l.StaffId == user.StaffId && i.IsDeleted == false
                              select new
                              {

                                  companyname = m.Name,
                                  //companyplan = n.Planfriendlyname,
                                  benefitname = j.Name,
                                  benefitcategory = j.CategoryName,
                                  benefitdescription = j.Description,
                                  benefitlimit = j.Benefitlimit,
                                  categoryname = j.CategoryName,
                                  status = j.Status,
                                  createdOn = i.CreatedOn

                              });

            return View(CompanyBenefits);

            //return View(await _context.CompanyBenefit.ToListAsync());
        }

        // GET: CompanyBenefits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyBenefit = await _context.CompanyBenefit
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyBenefit == null)
            {
                return NotFound();
            }

            return View(companyBenefit);
        }

        // GET: CompanyBenefits/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CompanyBenefits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Companyid,CompanyPlanid,BenefitId,BenefitLimit,Guid,CreatedOn,UpdatedOn,IsDeleted,SiteId")] CompanyBenefit companyBenefit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companyBenefit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(companyBenefit);
        }

        // GET: CompanyBenefits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyBenefit = await _context.CompanyBenefit.FindAsync(id);
            if (companyBenefit == null)
            {
                return NotFound();
            }
            return View(companyBenefit);
        }

        // POST: CompanyBenefits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Companyid,CompanyPlanid,BenefitId,BenefitLimit,Guid,CreatedOn,UpdatedOn,IsDeleted,SiteId")] CompanyBenefit companyBenefit)
        {
            if (id != companyBenefit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companyBenefit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyBenefitExists(companyBenefit.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(companyBenefit);
        }

        // GET: CompanyBenefits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyBenefit = await _context.CompanyBenefit
                .FirstOrDefaultAsync(m => m.Id == id);
            if (companyBenefit == null)
            {
                return NotFound();
            }

            return View(companyBenefit);
        }

        // POST: CompanyBenefits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companyBenefit = await _context.CompanyBenefit.FindAsync(id);
            _context.CompanyBenefit.Remove(companyBenefit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyBenefitExists(int id)
        {
            return _context.CompanyBenefit.Any(e => e.Id == id);
        }
    }
}
