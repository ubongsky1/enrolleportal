﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;
using NovoEnrollee.Models;

namespace NovoDiagnosis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClaimsController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ClaimsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }



        // GET: Dependents
        public async Task<IActionResult> GetClaims()
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);
            var claims = (from a in _context.Staffs
                         join b in _context.Enrollees on a.Profileid equals b.Parentid
                         join c in _context.Providers on b.Primaryprovider equals c.Id
                         join d in _context.Claims on b.Policynumber equals d.EnrolleePolicyNumber
                           where b.Policynumber.Contains(d.EnrolleePolicyNumber)
                         select new
                             {
                             EnrolleeFullname = a.StaffFullname,
                             EnrolleePolicyNumber = b.Policynumber,
                             CompanyName = d.EnrolleeCompanyName,
                             Date = d.ServiceDate,
                             ProviderName = c.Name,
                             AdmittedOn = d.AdmissionDate,
                             DischargedOn = d.DischargeDate,
                             MedicalDiagnosis = d.Diagnosis

                         }).ToList();


            return Ok(claims);
        }


    }
}
   
