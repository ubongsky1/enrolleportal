﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NovoDiagnosis.Data;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private static Random random = new Random();
        const string SessionName = "_Name";
        const string SessionAge = "_Age";


        public HomeController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        public async Task <IActionResult> Index()
        {
            

            //var user = await _userManager.GetUserAsync(User);
            //string myid = _context.Staffs.Where(x => x.StaffId == user.StaffId).Select(x => x.StaffId).ToString();
            //string policynumber = _context.Enrollees.Where(x => x.Id == "myid").Select(x => x.Policynumber).ToString();
            //HttpContext.Session.SetString("name", policynumber);
            return View();
        }

        public IActionResult About()
        {
            ViewBag.Name = HttpContext.Session.GetString("name");

            return View();
        }

        public IActionResult Portal(int? id)
        {
            TempData["CompanyName"] = _context.Company.Where(x => x.Id == id)
                                     .Select(x => x.Name).FirstOrDefault();

            var planid = _context.Company.Where(x => x.Id == id)
                                     .Select(x => x.Name).FirstOrDefault();
            return View();
        }



            public IActionResult Test()
        {
            //provides suggestions while you type into the field
            var name = HttpContext.Request.Query["term"].ToString();
            var StaffId = _context.Staffs.Where(c => c.StaffId.Contains(name)).Select(c => c.StaffId).ToList();
            return Ok(StaffId);
        }
        [HttpGet]
        public JsonResult Test2(string name)
        {
            //fill input fields when you select CNPJ CLIENTE
            var StaffFullname = _context.Staffs.Where(c => c.StaffId == name && c.StaffId != null).Select(c => c.StaffFullname).FirstOrDefault();
            return new JsonResult(StaffFullname);
        }
        public IActionResult State()
        {
            //provides suggestions while you type into the field
            var name = HttpContext.Request.Query["term"].ToString();
            var Stateid = _context.Lgas.Where(c => c.State == Convert.ToInt32(name)).Select(c => c.Name).ToList();
            return Ok(Stateid);
        }
        [HttpGet]
        public JsonResult State2(string name)
        {
            //fill input fields when you select CNPJ CLIENTE
            //ViewBag.Lga = new SelectList(_context.Lgas.Where(c => c.State == Convert.ToInt32(name)), "Id", "Name");
            var Lgaid = _context.Lgas.Where(c => c.State == Convert.ToInt32(name)).FirstOrDefault();
            /*_context.States.Where(c => c.Id == Convert.ToInt32(name)).Select(c => c.Name).ToList();*/


            return new JsonResult(Lgaid);
        }

        public IActionResult Contact()
        {

            string data;

            if (TempData["myData"] != null)
                data = TempData["myData"] as string;
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult SendPasswordResetLink(string UserName)
        {
            ApplicationUser user = _userManager.
                 FindByNameAsync(UserName).Result;

            if (user == null || !(_userManager.
                  IsEmailConfirmedAsync(user).Result))
            {
                ViewBag.Message = "Error while resetting your password!";
                return View("Error");
            }

            var token = _userManager.
                  GeneratePasswordResetTokenAsync(user).Result;

            var resetLink = Url.Action("ResetPassword",
                            "Account", new { token = token },
                             protocol: HttpContext.Request.Scheme);

            // code to email the above link
            // see the earlier article

            ViewBag.Message = "Password reset link has been sent to your email address!";
        return View("Login");

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult ResetForm()
        {
            return View();
        }
        public IActionResult ResetPassword(string token)
        {
            return View();
        }

        public IActionResult Faq()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ResetPassword
                (ResetPasswordViewModel obj)
        {
            ApplicationUser user = _userManager.
                         FindByNameAsync(obj.UserName).Result;

            IdentityResult result = _userManager.ResetPasswordAsync
                      (user, obj.Token, obj.Password).Result;
            if (result.Succeeded)
            {
                ViewBag.Message = "Password reset successful!";
                return View("Success");
            }
            else
            {
                ViewBag.Message = "Error while resetting the password!";
                return View("Error");
            }
        }
    }
}
