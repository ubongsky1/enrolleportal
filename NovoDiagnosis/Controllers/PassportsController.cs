﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Data;

namespace NovoDiagnosis.Controllers
{
    public class PassportsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PassportsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Passports
        public async Task<IActionResult> Index()
        {
            return View(await _context.EnrolleePassport.ToListAsync());
        }

        // GET: Passports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrolleePassport = await _context.EnrolleePassport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrolleePassport == null)
            {
                return NotFound();
            }

            return View(enrolleePassport);
        }

        // GET: Passports/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Passports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Enrolleeid,Enrolleepolicyno,Imgraw,CreatedOn,UpdatedOn,IsDeleted")] EnrolleePassport enrolleePassport)
        {
            if (ModelState.IsValid)
            {
                _context.Add(enrolleePassport);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(enrolleePassport);
        }

        // GET: Passports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrolleePassport = await _context.EnrolleePassport.FindAsync(id);
            if (enrolleePassport == null)
            {
                return NotFound();
            }
            return View(enrolleePassport);
        }

        // POST: Passports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Enrolleeid,Enrolleepolicyno,Imgraw,CreatedOn,UpdatedOn,IsDeleted")] EnrolleePassport enrolleePassport)
        {
            if (id != enrolleePassport.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(enrolleePassport);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EnrolleePassportExists(enrolleePassport.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(enrolleePassport);
        }

        // GET: Passports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enrolleePassport = await _context.EnrolleePassport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (enrolleePassport == null)
            {
                return NotFound();
            }

            return View(enrolleePassport);
        }

        // POST: Passports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var enrolleePassport = await _context.EnrolleePassport.FindAsync(id);
            _context.EnrolleePassport.Remove(enrolleePassport);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EnrolleePassportExists(int id)
        {
            return _context.EnrolleePassport.Any(e => e.Id == id);
        }
    }
}
