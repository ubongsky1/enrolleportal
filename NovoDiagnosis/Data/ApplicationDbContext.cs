﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NovoDiagnosis.Models;
using NovoEnrollee.Models;

namespace NovoEnrollee.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Company> Company { get; set; }
        public DbSet<CompanyBenefit> CompanyBenefit { get; set; }
        public DbSet<CompanyBranch> CompanyBranch { get; set; }
        public DbSet<CompanyPlan> CompanyPlan { get; set; }
        public DbSet<CompanySubsidiary> CompanySubsidiary { get; set; }
        public DbSet<DiagnosisDetails> DiagnosisDetails { get; set; }
        public DbSet<Enrollee> Enrollees { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<ProviderServices> ProviderServices { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<User> NovoUsers { get; set; }
        public DbSet<Lga> Lgas { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Support> Supports{ get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimBatch> ClaimBatches { get; set; }
        public DbSet<ClaimDrug> ClaimDrugs { get; set; }
        public DbSet<ClaimService> ClaimServices { get; set; }
        public DbSet<IncomingClaims> IncomingClaims { get; set; }
        public DbSet<Benefit> Benefits { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<EnrolleePassport> EnrolleePassport { get; set; }
        public DbSet<Refund> Refunds { get; set; }
        public DbSet<Complaint> Complaints { get; set; }

    }
}
